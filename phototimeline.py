#!/usr/bin/env python3

#Copyright 2018 Germain PHAM

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>. 

import tkinter
import tkinter.filedialog
import exiftool

def photoTimeline():
    print("i am from phototimeline.py")
    root = tkinter.Tk()
    filez =  tkinter.filedialog.askopenfilenames(initialdir = "$HOME",
                                                        title = "Select file",
                                                        filetypes = 
                                                        (("jpeg files","*.jpg"),("all files","*.*"))
                                                        )

    # print(root.tk.splitlist(filez))

    fileList = list(filez)
    print(fileList)

    with exiftool.ExifTool() as etool:
        metadata = etool.get_metadata_batch(fileList)
    for d in metadata:
        print("{:20.20} {:20.20}".format(d["SourceFile"],
                                d["EXIF:DateTimeOriginal"]))
 
if __name__ == "__main__":
    print("Executing as main program")
    print("Value of __name__ is: ", __name__)
    photoTimeline()


