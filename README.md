# photo-timeline

This is a tool for fixing the EXIF data of many photos that have wrong time 
information thanks to a (reference) timeline of reference photos

The first step of the project will be to display the reference photos on a 
single timeline. 
The second step will be to display a second timeline with the photos to fix. 

Further steps will be developed later. 